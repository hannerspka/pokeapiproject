
window.onload = function() {
    init()
}

const init = async () => {
    try {
        const pokemons = await getPokemons();
        printpokemons(pokemons.results)
        console.log(pokemons.results)
        } catch (error) {
            console.log(error);
     }
};

const getPokemons = async () => {
    try {
        const api = await fetch('https://pokeapi.co/api/v2/pokemon?limit=150&offset=0');
        const apiJson = await api.json(); 
        console.log(apiJson)
        return apiJson
     } catch (error) {
        console.log(error);
    };
};


const printpokemons = (arraypokemons) => {
    const ul = document.querySelector('ul')
    arraypokemons.forEach((pokemon, index) => {
        const list = document.createElement('li');
        list.className = "pokemon";
        list.textContent = pokemon.name
        
        const img = document.createElement('img');
        img.src = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/dream-world/${index + 1}.svg`
        img.alt = pokemon.name;

        list.appendChild(img)
        
        ul.appendChild(list)
    });
}




//https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index + 1}.png`